package org.protneut.common.utils;

import java.util.Map;

import org.protneut.common.objects.map.HierarchialHashMap;

public class MapUtils {

	
	// removing the starting and trailing '/' character and split by '/'
	public static String[] trimAndSplit(String s) {
		return s.replaceAll("([\\/])+$", "").replaceAll("^[\\/]", "").split("/");
	}
	
	
	public static <T> T goToWithoutNamespace(Map<String, Object> xmlMap, String path, Class<T> type) {
		// removing the starting and trailing '/' character
		String[] nodes = MapUtils.trimAndSplit(path);
		
		Map<String, Object> pointerMap = xmlMap;
		Map<String, Object> foundMap = null;
		String lastKey = null;
		boolean found = false;
		for (String nodeName : nodes) {
			found = false;
			foundMap = pointerMap;
			for(String nodeNameInMap : pointerMap.keySet()) {
				// getting the nodename from the path without namespace (/aa:aa/bb:bb -> bb  ;  /aa:aa/bb -> bb)
				String originalKey = nodeNameInMap;
				String key = simplifyKey(originalKey);
				// if the name of the key is equal to nodeName
				if (key.equals(nodeName)) {
					// if the value in the map is a HierarchialHashMap
					if (pointerMap.get(originalKey) != null) {
						if (pointerMap.get(originalKey) instanceof Map) {
							pointerMap = (Map<String, Object>)pointerMap.get(originalKey);
						}
						found = true;
						lastKey = nodeNameInMap;
						break;
					}
				}
			}
		}
		if (!found) {
			return null;
		} else {
			return type.cast(foundMap.get(lastKey));
		}
	}

	
	private static String simplifyKey(final String _key) {
		String key = _key;
		if (key.lastIndexOf("/") != -1) {
			key = key.substring(key.lastIndexOf("/") + 1);
			if (key.lastIndexOf(":") != -1) {
				key = key.substring(key.lastIndexOf(":") + 1);
			}
		} else if (key.lastIndexOf(":") != -1) {
			key = key.substring(key.lastIndexOf(":") + 1);
		}

		return key;
	}
}
