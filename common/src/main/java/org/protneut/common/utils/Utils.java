package org.protneut.common.utils;

import java.util.Map;

public class Utils {

	
	public static boolean isNull(Object o) {
		return o == null;
	}
	
	public static void checkNull(Object o, String paramName) {
		if (isNull(o)) {
			throw new IllegalArgumentException("The "+paramName+" cannot be null!");
		}
	}


	public static boolean isEmpty(String s) {
		return isNull(s) || s.length() == 0;
	}
	
	public static void checkEmpty(String s, String paramName) {
		if (isEmpty(s)) {
			throw new IllegalArgumentException("The "+paramName+" cannot be empty!");
		}
	}
	
	
	public static boolean isEmpty(Map<?, ?> m) {
		return isNull(m) || m.isEmpty();
	}
	
	public static void checkEmpty(Map<?, ?> m, String paramName) {
		if (isEmpty(m)) {
			throw new IllegalArgumentException("The "+paramName+" cannot be empty!");
		}
	}

}
