package org.protneut.server.cache;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.protneut.server.beans.workflow.WorkflowBean;
import org.protneut.server.common.beans.cache.DataCacheBean;
import org.protneut.server.common.beans.cache.ServiceCacheBean;
import org.protneut.server.common.dao.highlevel.cache.ICacheBeanDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ServiceStorageManager {

	
	private static final Logger LOGGER = LoggerFactory.getLogger(ServiceStorageManager.class);
	
	private ICacheBeanDAO cacheBeanDAO;
	
	
private static boolean isLoad = false;
	public ServiceStorageManager() {
}
	
	public WorkflowBean getServiceInfo(final WorkflowBean _bean) {
try {
	if (!isLoad) {
isLoad=true;
// ******************************** service: crm/getCustomerDetailsById *********************
ServiceCacheBean serviceCacheBean = new ServiceCacheBean();

serviceCacheBean.setName("crm/getCustomerDetailsById");

List<String> endpoints = new ArrayList<String>();
endpoints.add("http://12.13.14.15:8080/sg");
endpoints.add("http://localhost:8088/mockcrmBinding");
serviceCacheBean.setEndpoints(endpoints);

serviceCacheBean.setType("webservice");

serviceCacheBean.setRequestString("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:crm=\"http://www.acme.com/crm\"><soapenv:Header/><soapenv:Body><crm:GetCustomerDetailsByIdRequest><id>${protneutParam.id}</id></crm:GetCustomerDetailsByIdRequest></soapenv:Body></soapenv:Envelope>");

serviceCacheBean.setSoapAction("http://www.acme.com/crm/getCustomerDetailsById");

Map<String, String> namespaces = new HashMap<String, String>();
namespaces.put("soapenv", "http://schemas.xmlsoap.org/soap/envelope/");
namespaces.put("crm", "http://www.acme.com/crm");
serviceCacheBean.setNamespaces(namespaces);

cacheBeanDAO.put(serviceCacheBean.getName(), serviceCacheBean);

//******************************** service: crm/getCustomerDetailsById *********************
serviceCacheBean = new ServiceCacheBean();

serviceCacheBean.setName("GetAddressForPostCode_V4");

endpoints = new ArrayList<String>();
endpoints.add("http://localhost:8083/mockGetAddressForPostCode");
serviceCacheBean.setEndpoints(endpoints);

serviceCacheBean.setType("webservice");

serviceCacheBean.setRequestString("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ns=\"http://virginmedia/schema/GetAddressForPostCode/4/0\"  xmlns:ns1=\"http://virginmedia/schema/RequestHeader/1/4\" xmlns:ns2=\"http://virginmedia/schema/BaseType/1/3\"><soapenv:Header/><soapenv:Body><ns:GetAddressForPostCodeRequest><ns:requestHeader><ns1:id>?</ns1:id><ns1:source>?</ns1:source><ns1:user><ns2:userId>?</ns2:userId><ns2:credentials>?</ns2:credentials></ns1:user></ns:requestHeader><ns:postalAddress><#if protneutParam.buildingName??><ns:buildingName>${protneutParam.buildingName}</ns:buildingName></#if><#if protneutParam.country??><ns:country>${protneutParam.country}</ns:country></#if><ns:postcode>${protneutParam.postcode}</ns:postcode><ns:addressIdentification><ns:identifierType>${protneutParam.addressIdentification.identifierType}</ns:identifierType><ns:addressIdentifier>${protneutParam.addressIdentification.addressIdentifier}</ns:addressIdentifier></ns:addressIdentification></ns:postalAddress></ns:GetAddressForPostCodeRequest></soapenv:Body></soapenv:Envelope>");

serviceCacheBean.setSoapAction("GetAddressForPostCode");

namespaces = new HashMap<String, String>();
namespaces.put("soapenv", "http://schemas.xmlsoap.org/soap/envelope/");
namespaces.put("ns", "http://virginmedia/schema/GetAddressForPostCode/4/0");
//namespaces.put("ns1", "http://virginmedia/schema/RequestHeader/1/4");
namespaces.put("ns1", "http://virginmedia/schema/ResponseHeader/2/1");
//namespaces.put("ns2", "http://virginmedia/schema/BaseType/1/3");
namespaces.put("ns2", "http://virginmedia/schema/faults/2/1");
serviceCacheBean.setNamespaces(namespaces);

cacheBeanDAO.put(serviceCacheBean.getName(), serviceCacheBean);

// ******************************** data: customer->name *********************
DataCacheBean dataCacheBean = new DataCacheBean();
dataCacheBean.setDataName("customer->name");

List<Map<String, String>> extracts = new ArrayList<Map<String,String>>();
Map<String, String> extract = new HashMap<String, String>();
extract.put("type", "xpath");
extract.put("from", "/soapenv:Envelope/soapenv:Body/crm:GetCustomerDetailsByIdResponse/personal/fullName/text()");
extracts.add(extract);
dataCacheBean.setExtracts(extracts);

dataCacheBean.setServiceName("crm/getCustomerDetailsById");

cacheBeanDAO.put(dataCacheBean.getDataName(), dataCacheBean);

//******************************** data: address->getAddressForPostCode->postalAddress_short *********************
dataCacheBean = new DataCacheBean();
dataCacheBean.setDataName("address->getAddressForPostCode->postalAddress_short");

extracts = new ArrayList<Map<String,String>>();
extract = new HashMap<String, String>();
extract.put("type", "xpath");
extract.put("from", "/soapenv:Envelope/soapenv:Body/ns:GetAddressForPostCodeResponse/ns:postalAddress/ns:displayAddress");
//extract.put("to", "/*address/displayAddress");
extracts.add(extract);
extract = new HashMap<String, String>();
extract.put("type", "xpath");
extract.put("from", "/soapenv:Envelope/soapenv:Body/ns:GetAddressForPostCodeResponse/ns:postalAddress/ns:buildingName");
//extract.put("to", "/*address/buildingName");
extract = new HashMap<String, String>();
extract.put("type", "xpath");
// TODO *************************************************************************************************************************
// from :	/soapenv:Envelope/soapenv:Body/ns:GetAddressForPostCodeResponse/ns:postalAddress/ns:addressIdentification/ns:identifierType
// to :		/*address/*identification/identifierType
// TODO so the problem is with the * is that as according to the current state of the data in the DB the original map (the result of XmlToMapConverter)
//    may contain one or two lists (when building the map and there is only one element in the list then I'll remove the list and just put that one
//    element to the parent map) -> 
//      - two lists: no problem
//      - one list: problem because I don't know if I have to multiply the 'address' or 'identification' element
// THE SOLUTION: binding accurately the 'from' part with the 'to' part
//   for example:  /soapenv:Envelope/soapenv:Body/ns:GetAddressForPostCodeResponse/ns:postalAddress -> /address
//                 /soapenv:Envelope/soapenv:Body/ns:GetAddressForPostCodeResponse/ns:postalAddress/ns:addressIdentification -> /address/identification
//   need some very interactive UI component in the web application!
extract.put("from", "/soapenv:Envelope/soapenv:Body/ns:GetAddressForPostCodeResponse/ns:postalAddress/ns:addressIdentification/ns:identifierType");
//extract.put("to", "/*address/*identification/identifierType");
extracts.add(extract);
dataCacheBean.setExtracts(extracts);

dataCacheBean.setServiceName("GetAddressForPostCode_V4");

cacheBeanDAO.put(dataCacheBean.getDataName(), dataCacheBean);
}
} catch(Throwable t) {
LOGGER.error("Exception while inserting data to ehcache!", t);
}
		
		LOGGER.info("The method 'getServiceInfo' has been called.");
		WorkflowBean bean = _bean.clone();

		// getting the data cache bean from the cache 
		DataCacheBean dataCacheBean = cacheBeanDAO.getDataCacheBean(bean.getDataName());
		bean.setExtracts(dataCacheBean.getExtracts());

		// getting the service cache bean from the cache
		ServiceCacheBean serviceCacheBean = cacheBeanDAO.getServiceDataCacheBean(dataCacheBean.getServiceName());

		bean.setEndpoint(serviceCacheBean.getEndpoints());
		bean.setRequestString(serviceCacheBean.getRequestString());
		bean.setServiceType(serviceCacheBean.getType());
		bean.setSoapAction(serviceCacheBean.getSoapAction());
		bean.setNamespaces(serviceCacheBean.getNamespaces());
		
		LOGGER.debug("The WorkflowBean being filled : {}", bean);
		
		return bean;
	}

	
	public ICacheBeanDAO getCacheBeanDAO() {
		return cacheBeanDAO;
	}

	public void setCacheBeanDAO(ICacheBeanDAO cacheBeanDAO) {
		this.cacheBeanDAO = cacheBeanDAO;
	}

	

}
