package org.protneut.server.extract;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.protneut.common.objects.map.HierarchialHashMap;
import org.protneut.common.utils.MapUtils;
import org.protneut.server.beans.workflow.WorkflowBean;
import org.protneut.server.common.exceptions.ExternalServiceException;
import org.protneut.server.extract.IXmlToMapConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ximpleware.EOFException;
import com.ximpleware.EncodingException;
import com.ximpleware.EntityException;
import com.ximpleware.NavException;
import com.ximpleware.ParseException;
import com.ximpleware.XPathEvalException;
import com.ximpleware.XPathParseException;

public class ResponseExtractor {

	
	private static final Logger LOGGER = LoggerFactory.getLogger(ResponseExtractor.class);
	
	private IXmlToMapConverter xmlToMapConverter;
	private List<String> extracts = null;
	
	
	public WorkflowBean extract(WorkflowBean _bean) throws EncodingException, EOFException, EntityException, NavException, ParseException, XPathParseException, XPathEvalException, ExternalServiceException {
		LOGGER.info("The method 'extract' has been called.");
		WorkflowBean bean = _bean.clone();
		
		// converting the extracts to a single List<String>
		setExtracts(bean.getExtracts());
		
		// converting the XML to Map
		xmlToMapConverter.start(bean.getResponseString(), bean.getNamespaces());
		HierarchialHashMap<String, Object> xmlMap = xmlToMapConverter.getMap();
		LOGGER.debug("Map representation of the response XML: {}", xmlMap);

		// checking if the response contains SOAP Fault
		checkingSOAPFault(xmlMap, "Envelope");
		
		// only keeping the elements in the map that are needed to retrieve
		removeUnnecessaryNodes(xmlMap);
		LOGGER.debug("The map after removing the unnecessary nodes: {}", xmlMap);
		
		// using simple keys in the map (not the whole xpath)
		makeKeysSimple(xmlMap);
		LOGGER.debug("The map after making the keys simple: {}", xmlMap);
		
		bean.setExtractedValues(xmlMap);
		
		return bean;
	}

	
	private void checkingSOAPFault(HierarchialHashMap<String, Object> xmlMap, String nodeName) throws ExternalServiceException {
		Map<String, Object> faultMap = MapUtils.goToWithoutNamespace(xmlMap, "/Envelope/Body/Fault", Map.class);
		if (faultMap != null) {
			String faultcode = (String) MapUtils.goToWithoutNamespace(faultMap, "faultcode", String.class);
			String faultstring = (String) MapUtils.goToWithoutNamespace(faultMap, "faultstring", String.class);
			throw new ExternalServiceException("SOAP Fault occurred! faultcode: " + faultcode + "; faultstring: " + faultstring);
		}
	}


	private void makeKeysSimple(HierarchialHashMap<String, Object> xmlMap) {
		Iterator<Entry<String, Object>> iterator = xmlMap.entrySet().iterator();
		Map<String, Object> temp = new HashMap<String, Object>();
		while (iterator.hasNext()) {
			Entry<String, Object> item = iterator.next();
			String key = item.getKey();
			Object value = item.getValue();
			temp.put(key.substring(key.lastIndexOf("/")+1), value);
			iterator.remove();
			
			if (value instanceof HierarchialHashMap) {
				makeKeysSimple((HierarchialHashMap<String, Object>)value);
			} else if (value instanceof List) {
				List list = (List)value;
				for(Object o : list) {
					if (o instanceof HierarchialHashMap) {
						makeKeysSimple((HierarchialHashMap<String, Object>)o);
					}
				}
			}
		}
		xmlMap.putAll(temp);
	}

	
	private void removeUnnecessaryNodes(HierarchialHashMap<String, Object> map) {
		Iterator<Entry<String, Object>> iterator = map.entrySet().iterator();
		while (iterator.hasNext()) {
			Entry<String, Object> item = iterator.next();
			String key = item.getKey();
			Object value = item.getValue();
			// if mapAll.get(key) contains
			//   - String or
			//   - empty HashMap
			// then the element is candidate to be removed
			if (     value instanceof String || 
					(value instanceof HashMap && ((HashMap<?, ?>)value).isEmpty()) ) {
				// if the extracts list doesn't contain the key then it has to be removed
				if (!extracts.contains(key)) {
					iterator.remove();
					// removing the empty parent nodes above this node
					removeEmptyParentNodes(map);
				}
			}
			// if the value is a HierarchialHashMap then dig deeper (-> recursive call)
			else if (value instanceof HierarchialHashMap) {
				removeUnnecessaryNodes((HierarchialHashMap<String, Object>)value);
			}
			// if the value is a List then all the elements of the list have to be examined
			else if (value instanceof List) {
				for(Object o : (List)value) {
					// if a HierarchialHashMap then dig deeper
					if (o instanceof HierarchialHashMap) {
						removeUnnecessaryNodes((HierarchialHashMap<String, Object>)o);
					}
					// if String or empty HashMap then the element is candidate to be removed
					if (     o instanceof String || 
							(o instanceof HashMap && ((HashMap<?, ?>)o).isEmpty()) ) {
						// if the extracts list doesn't contain the key then it has to be removed
						if (!extracts.contains(key)) {
							((List)value).remove(o);
						}
					}
				}
				// if the list has one value then we don't need list...
				if (((List)value).size() == 1) {
					map.remove(key);
					// not putting back the empty Map (it was already removed in removeEmptyParentNodes but because of the recursive calls it exists here...)
					Object o = ((List)value).get(0);
					if ( o instanceof Map &&  ((Map)o).isEmpty() ) {
					} else {
						map.put(key, o);
					}
				}
				// if the list is empty then it has to be removed from the map
				if (((List)value).size() == 0) {
					// removing the empty parent nodes above this node
					map.remove(key);
					removeEmptyParentNodes(map);
				}
			} else {
				throw new RuntimeException("Well I really don't know what is this element... -> " + value.getClass() + " : " + value.toString());
			}
		}
		
	}


	private void removeEmptyParentNodes(final HierarchialHashMap<String, Object> map) {
		HierarchialHashMap<String, Object> parentMap = map;
		while(parentMap!= null && parentMap.isEmpty()) {
			String keyToBeRemoved = parentMap.getCurrentXpath();
			parentMap = parentMap.getParent();
			if (parentMap != null) {
				if (parentMap.get(keyToBeRemoved) instanceof List) {
					List list = (List)parentMap.get(keyToBeRemoved);
					// if there is only one element in the list then the whole key can be deleted
					if (list.size() == 1) {
						parentMap.remove(keyToBeRemoved);
					} else {
						for(Object o : list) {
							if (o instanceof Map) {
								if (((Map)o).isEmpty()) {
									list.remove(o);
								}
							}
						}
						// if all the elements of the list were removed then the key can be removed from the map
						if (list.size() == 0) {
							parentMap.remove(keyToBeRemoved);
						}
					}
				} else {
					parentMap.remove(keyToBeRemoved);
				}
			}
		}
	}


	private void setExtracts(List<Map<String, String>> extractsListMap) {
		extracts = new ArrayList<String>();
		
		for(Map<String, String> map : extractsListMap) {
			extracts.add(map.get("from"));
		}
	}


	public IXmlToMapConverter getXmlToMapConverter() {
		return xmlToMapConverter;
	}

	public void setXmlToMapConverter(IXmlToMapConverter xmlToMapConverter) {
		this.xmlToMapConverter = xmlToMapConverter;
	}


}