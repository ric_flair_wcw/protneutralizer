package org.protneut.server.call;

import java.io.IOException;
import java.util.Calendar;

import org.protneut.server.beans.workflow.WorkflowBean;
import org.protneut.server.common.exceptions.ExternalServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;

public class ServiceCall {

	
	private static final Logger LOGGER = LoggerFactory.getLogger(ServiceCall.class);
	
	private HttpClient httpClient = null;
	private EndpointsQueue endpointsQueue = null;
	
	
	public WorkflowBean call(final WorkflowBean _bean) throws IOException, ExternalServiceException {
		LOGGER.info("The method 'call' has been called.");
		WorkflowBean bean = _bean.clone();
		
		endpointsQueue.initQueue(bean.getEndpoint());
		bean.setStartCallExternalServiceTime(Calendar.getInstance());
		
		// calling the service
		HttpResponse httpResponse = callWithLoadBalancing(bean.getRequestString(), bean.getSoapAction(), bean.getDataName());
		bean.setEndCallExternalServiceTime(Calendar.getInstance());
		
		// extracting the SOAP response string
		if (httpResponse == null) {
			throw new ExternalServiceException("No response coming back from the service for the data '"+bean.getDataName()+"'!");
		}
		HttpEntity httpEntity = httpResponse.getEntity();
        if (httpEntity != null) {
        	String responseText = EntityUtils.toString(httpEntity);
        	LOGGER.debug("The response arrived from the service is {}", responseText);
        	bean.setResponseString(responseText);
        } else {
			throw new ExternalServiceException("No response coming back from the service for the data '"+bean.getDataName()+"'!");
        }
        
        return bean;
	}


	// TODO using round-robin: at the moment always the first endpoint is called first
	private HttpResponse callWithLoadBalancing(String requestString, String soapAction, String dataName) throws IOException, ClientProtocolException, ExternalServiceException {
		String endpoint = endpointsQueue.next();
		
		if (endpoint == null) {
			throw new ExternalServiceException("No service available for the data '"+dataName+"'!");
		}
		
		// request parameters and other properties
		StringEntity stringEntity = new StringEntity(requestString, "UTF-8");
		stringEntity.setChunked(true);
		
		HttpPost httpPost = new HttpPost(endpoint);
		httpPost.setEntity(stringEntity);
		httpPost.addHeader("Accept" , "text/xml");
		httpPost.addHeader("SOAPAction", soapAction);
		
		// calling the web service
		HttpResponse httpResponse = null;
		Calendar startTime = Calendar.getInstance();
		LOGGER.trace("Calling the service on {} with \n{}", endpoint, requestString);
		try {
			httpResponse = httpClient.execute(httpPost);
			long ms = Calendar.getInstance().getTimeInMillis() - startTime.getTimeInMillis();
	        LOGGER.trace("Called the web service service. The time needed to call it is {} ms. The response is\n{}", ms, httpResponse.getEntity());
		} catch(HttpHostConnectException e) {
			LOGGER.warn("The endpoint is not available! Calling the next one.", e);
			httpResponse = callWithLoadBalancing(requestString, soapAction, dataName);
		} catch(ConnectTimeoutException e) {
			LOGGER.warn("The endpoint is not available! Calling the next one. {}", e);
			httpResponse = callWithLoadBalancing(requestString, soapAction, dataName);
		}

        return httpResponse;
	}


	public HttpClient getHttpClient() {
		return httpClient;
	}

	public void setHttpClient(HttpClient httpClient) {
		this.httpClient = httpClient;
	}

	public EndpointsQueue getEndpointsQueue() {
		return endpointsQueue;
	}

	public void setEndpointsQueue(EndpointsQueue endpointsQueue) {
		this.endpointsQueue = endpointsQueue;
	}
	
}
