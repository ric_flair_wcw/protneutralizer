package org.protneut.server.call;

import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import org.protneut.server.beans.workflow.WorkflowBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import freemarker.cache.StringTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

public class ServiceRequestCreator {

	
	private static final Logger LOGGER = LoggerFactory.getLogger(ServiceRequestCreator.class);
	
	private Configuration freemarkerConfiguration;
	
	
	public WorkflowBean create(WorkflowBean _bean) throws IOException, TemplateException {
		LOGGER.info("The method 'create' has been called.");
		
		WorkflowBean bean = _bean.clone();
		
		// adding the request string to a class (StringTemplateLoader) that will be used as an input for freemarkerConfiguration
		StringTemplateLoader stringTemplateLoader = new StringTemplateLoader();
		stringTemplateLoader.putTemplate("requestString", bean.getRequestString());
		
		freemarkerConfiguration.setTemplateLoader(stringTemplateLoader);
		
		// getting the freemarker template
		Template template = freemarkerConfiguration.getTemplate("requestString");
	
		// creating the complete request string
		StringWriter requestStringWriter = new StringWriter();
		template.process(createDataModel(bean), requestStringWriter);
		
		bean.setRequestString(requestStringWriter.toString());
		
		LOGGER.debug("The request string : {}", bean.getRequestString());
		
		return bean;
	}


	private Map<String, Object> createDataModel(WorkflowBean bean) {
		Map<String, Object> params = new HashMap<String, Object>();
		if (bean.getParams() == null) {
			params.put("id", bean.getId());
		} else {
			params.putAll(bean.getParams());
		}
		
		Map<String, Object> main = new HashMap<String, Object>();
		main.put("protneutParam", params);
		LOGGER.trace("The Freemarker data model : {}", main);
		return main;
	}


	public Configuration getFreemarkerConfiguration() {
		return freemarkerConfiguration;
	}

	public void setFreemarkerConfiguration(Configuration freemarkerConfiguration) {
		this.freemarkerConfiguration = freemarkerConfiguration;
	}
}
