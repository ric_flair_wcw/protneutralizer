package org.protneut.server.beans.workflow;

import java.util.Calendar;
import java.util.List;
import java.util.Map;

public class WorkflowBean {

	
	// original data - coming from REST request
	private String dataName;
	private String id;
	private Map<String, Object> params;
	
	// service call info
	//		containing the request string; e.g. the SOAP request in case of SOAP ws and so on
	private String requestString;
	//		containing the response string that arrived from the called service
	private String responseString;
	//		the endpoint of the service to be called
	private List<String> endpoints;
	//		the type of the service (webservice, rest, ...)
	private String serviceType;
	//		the path of the elements that have to be retrieved
	private List<Map<String,String>> extracts; // 1st - type (e.g. xpath), 2nd - expression (e.g. /customer/nameFull)
	//		the namespaces for xpath
	private Map<String,String> namespaces;
	//		soapAction for calling webservice
	private String soapAction;
	//		the extracted value from the response that will be put to the REST response
	//				Map<String, Object> if json has to be sent back
	//				String if only one values has to be sent back
	private Object extractedValues;
	
	// dates
	//		arrival of the request to server-engine
	private Calendar reqArrivalTime;
	//		departure of the request from server-engine
	private Calendar reqDepartureTime;
	//		start date of calling the external service
	private Calendar startCallExternalServiceTime;
	//		end date of calling the external service
	private Calendar endCallExternalServiceTime;
	
	
	public WorkflowBean(String dataName, String id) {
		this.dataName = dataName;
		this.id = id;
		reqArrivalTime = Calendar.getInstance();
	}
	
	public WorkflowBean(String dataName, Map<String, Object> params) {
		this.dataName = dataName;
		this.params = params;
	}

	public WorkflowBean(String dataName, String id, Map<String, Object> params, String requestString, String responseString, List<String> endpoint, String serviceType, 
			List<Map<String,String>> extracts, String soapAction, Object extractedValues, Map<String, String> namespaces, Calendar reqArrivalTime, Calendar reqDepartureTime,
			Calendar startCallExternalServiceTime, Calendar endCallExternalServiceTime) {
		this.dataName = dataName;
		this.id = id;
		this.params = params;
		this.requestString = requestString;
		this.responseString = responseString;
		this.endpoints = endpoint;
		this.serviceType = serviceType;
		this.extracts = extracts;
		this.soapAction = soapAction;
		this.extractedValues = extractedValues;
		this.namespaces = namespaces;
		this.reqArrivalTime = reqArrivalTime;
		this.reqDepartureTime = reqDepartureTime;
		this.startCallExternalServiceTime = startCallExternalServiceTime;
		this.endCallExternalServiceTime = endCallExternalServiceTime;
	}


	public String getDataName() {
		return dataName;
	}

	public void setDataName(String dataName) {
		this.dataName = dataName;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Map<String, Object> getParams() {
		return params;
	}

	public void setParams(Map<String, Object> params) {
		this.params = params;
	}

	public String getRequestString() {
		return requestString;
	}

	public void setRequestString(String requestString) {
		this.requestString = requestString;
	}

	public String getResponseString() {
		return responseString;
	}

	public void setResponseString(String responseString) {
		this.responseString = responseString;
	}

	public List<String> getEndpoint() {
		return endpoints;
	}

	public void setEndpoint(List<String> endpoint) {
		this.endpoints = endpoint;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	
	public List<Map<String,String>> getExtracts() {
		return extracts;
	}

	public void setExtracts(List<Map<String,String>> extracts) {
		this.extracts = extracts;
	}

	public String getSoapAction() {
		return soapAction;
	}

	public void setSoapAction(String soapAction) {
		this.soapAction = soapAction;
	}

	public Object getExtractedValues() {
		return extractedValues;
	}

	public void setExtractedValues(Object extractedValues) {
		this.extractedValues = extractedValues;
	}

	public Map<String,String> getNamespaces() {
		return namespaces;
	}

	public void setNamespaces(Map<String,String> namespaces) {
		this.namespaces = namespaces;
	}

	public Calendar getReqDepartureTime() {
		return reqDepartureTime;
	}

	public void setReqDepartureTime(Calendar reqDepartureTime) {
		this.reqDepartureTime = reqDepartureTime;
	}

	public Calendar getStartCallExternalServiceTime() {
		return startCallExternalServiceTime;
	}

	public void setStartCallExternalServiceTime(
			Calendar startCallExternalServiceTime) {
		this.startCallExternalServiceTime = startCallExternalServiceTime;
	}

	public Calendar getEndCallExternalServiceTime() {
		return endCallExternalServiceTime;
	}

	public void setEndCallExternalServiceTime(Calendar endCallExternalServiceTime) {
		this.endCallExternalServiceTime = endCallExternalServiceTime;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(this.getClass().getName()).append("[");
		sb.append("dataName=").append(dataName);
		sb.append(", id=").append(id);
		sb.append(", requestString=").append(requestString);
		sb.append(", responseString=").append(responseString);
		sb.append(", endpoints=").append(endpoints);
		sb.append(", serviceType=").append(serviceType);
		sb.append(", params=").append(params);
		sb.append(", extracts=").append(extracts);
		sb.append(", soapAction=").append(soapAction);
		sb.append(", extractedValues=").append(extractedValues);
		sb.append(", namespaces=").append(namespaces);
		sb.append(", reqArrivalTime=").append(reqArrivalTime);
		sb.append(", reqDepartureTime=").append(reqDepartureTime);
		sb.append(", startCallExternalServiceTime=").append(startCallExternalServiceTime);
		sb.append(", endCallExternalServiceTime=").append(endCallExternalServiceTime);
		sb.append("]");
		return sb.toString();
	}

	public WorkflowBean clone() {
		return new WorkflowBean(dataName, id, params, requestString, responseString, endpoints, serviceType, extracts, soapAction, extractedValues, namespaces, reqArrivalTime, 
				reqDepartureTime, startCallExternalServiceTime, endCallExternalServiceTime);
	}

	
}
