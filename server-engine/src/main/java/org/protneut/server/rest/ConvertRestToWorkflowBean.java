package org.protneut.server.rest;

import java.util.Map;

import org.protneut.common.utils.Utils;
import org.protneut.server.beans.workflow.WorkflowBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;

public class ConvertRestToWorkflowBean {

	
	private static final Logger LOGGER = LoggerFactory.getLogger(ConvertRestToWorkflowBean.class);
	
	
	public WorkflowBean convert(Message<?> inMessage) {
		LOGGER.debug("The method 'convert' has been called. " + inMessage);
		
		MessageHeaders headers = inMessage.getHeaders();
		
		String dataName = (String) headers.get("protNeut_dataName");
		Utils.checkEmpty(dataName, "dataName");
		
		String id = null;
		WorkflowBean bean = null;
		if (headers.get("protNeut_id") != null) {
			id = (String) headers.get("protNeut_id");
			bean = new WorkflowBean(dataName, id);
		} else {
			Map<String, Object> params = (Map<String, Object>)inMessage.getPayload();
			bean = new WorkflowBean(dataName, params);
		}
		
		LOGGER.trace("The created WorkflowBean: {}", bean);
		
		return bean;
	}
	
}
