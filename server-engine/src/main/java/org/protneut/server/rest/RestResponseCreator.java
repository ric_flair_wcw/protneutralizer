package org.protneut.server.rest;

import java.util.HashMap;
import java.util.Map;

import org.protneut.server.beans.workflow.WorkflowBean;
import org.protneut.server.extract.ResponseExtractor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHandlingException;
import org.springframework.messaging.support.GenericMessage;

public class RestResponseCreator {

	
	private static final Logger LOGGER = LoggerFactory.getLogger(RestResponseCreator.class);
	
	
	public Message<Object> create(WorkflowBean bean) {
		LOGGER.info("The method 'create' has been called.");
		
		Map<String,Object> messageMap = new HashMap<String, Object>();
		
		// if only one value has to be sent back then there is a string in the ExtractedValues
		if (bean.getExtractedValues() instanceof String) {
			return new GenericMessage<Object>((String)bean.getExtractedValues());
		}
		// if only json has to be sent back then there is a Map in the ExtractedValues
		else {
			Map<String, Object> extractedValues = (Map<String, Object>)bean.getExtractedValues();
			messageMap = extractedValues;
				
			return new GenericMessage<Object>(createLetter(messageMap, "ok"));
		}
	}

	
	public Message<Map<String,Object>> error(Message<MessageHandlingException> message) {
		LOGGER.info("The method 'error' has been called.");
		Map<String, Object> exceptionMap = new HashMap<String, Object>();
		String errorClassName = null;
		String errorMessage = null;
		
		if (message.getPayload().getClass().getName().equals("org.springframework.messaging.MessageHandlingException")) {
			errorClassName = message.getPayload().getCause().getClass().getName();
			errorMessage = message.getPayload().getCause().getMessage();
		} else {
			errorClassName = message.getPayload().getClass().getName();
			errorMessage = message.getPayload().getMessage();
		}
		exceptionMap.put("class", errorClassName);
		exceptionMap.put("message", errorMessage);
		
		return new GenericMessage<Map<String,Object>>(createLetter(exceptionMap, "error"));
	}
	
	
	private Map<String, Object> createLetter(Map<String, Object> bodyMap, String status) {
		Map<String, Object> letterMap = new HashMap<String, Object>();
		Map<String, String> statusMap = new HashMap<String, String>();
		
		statusMap.put("status", status);
		
		letterMap.put("header", statusMap);
		letterMap.put("body", bodyMap);
		
		return letterMap;
	}


}
