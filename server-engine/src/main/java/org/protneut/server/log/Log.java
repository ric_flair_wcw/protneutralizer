package org.protneut.server.log;

import java.util.Calendar;
import java.util.Iterator;

import org.protneut.server.beans.workflow.WorkflowBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.integration.store.MessageGroup;
import org.springframework.integration.store.SimpleMessageStore;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHandlingException;

public class Log {

	
	private static final Logger LOGGER = LoggerFactory.getLogger(Log.class);
	
	private SimpleMessageStore simpleMessageStore;
	
	
	public void doIt(Message<MessageHandlingException> m) {
		LOGGER.info("Yeah, just wrinting this: {}", m);

		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			LOGGER.info("Oh no, error... :" + e);
		}
		
		WorkflowBean workflowBean =  (WorkflowBean) m.getHeaders().get("storedWorkflowBean");
		// IT IS IMPORTANT to leave in the final version of the class too!!!
		workflowBean.setReqDepartureTime(Calendar.getInstance());
		
//		WorkflowBean workflowBean =  (WorkflowBean) ((org.springframework.messaging.Message)m.getHeaders().get("storedWorkflowBean")).getPayload();
		LOGGER.info("original-payload: {}", workflowBean);
	
		LOGGER.info("Ending this...");
	}


	public SimpleMessageStore getSimpleMessageStore() {
		return simpleMessageStore;
	}


	public void setSimpleMessageStore(SimpleMessageStore simpleMessageStore) {
		this.simpleMessageStore = simpleMessageStore;
	}
}
