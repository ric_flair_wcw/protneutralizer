package org.protneut.clientframework.call;

import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.client.utils.URIBuilder;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.protneut.clientframework.EndpointsQueue;
import org.protneut.clientframework.exception.ClientFrameworkException;
import org.protneut.clientframework.exception.ServerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

public class RestClient {

	
    private static final Logger LOGGER = LoggerFactory.getLogger(RestClient.class);
    
	private RestTemplate restTemplate;
	private EndpointsQueue endpointsQueue;
	
	
	//http://stackoverflow.com/questions/18328492/resttemplate-jackson
	//http://stackoverflow.com/questions/21316641/how-to-properly-setup-a-post-spring-rest-template-by-passing-in-parameters-and-r
	public ResponseEntity<String> forward(String dataName, String id, Map<String, Object> params) throws RestClientException, ClientFrameworkException, ServerException {
		ResponseEntity<String> response = null;
		
		response = callWithLoadBalancing(endpointsQueue.next(), dataName, id, params);
		
		return response;
	}


	private ResponseEntity<String> callWithLoadBalancing(String _endpoint, String dataName, String id, Map<String, Object> params) throws ClientFrameworkException, ServerException {
		String endpoint = _endpoint;
		ResponseEntity<String> response = null;

		if (endpoint == null) {
			throw new ClientFrameworkException("The REST service is not available!");
		}
		
		Calendar startTime = Calendar.getInstance();
		
		try {
			// if json is null then getting a data based on id
			if (params == null) {
				// Exception in thread "main" org.springframework.web.client.HttpClientErrorException: 404 Not Found
				LOGGER.debug("Calling the REST service on {} ...", endpoint);
				response = restTemplate.postForEntity(endpoint, null, String.class, dataName, id);
			}
			// else send the json string to the endpoint
			else {
				endpoint = UriComponentsBuilder.fromUriString(endpoint.replace("/{id}", "")).build().expand(dataName).toUriString();
				LOGGER.debug("Calling the REST service on {} ...", endpoint);
				if (params.get("dataName") != null) {
					throw new ClientFrameworkException("The parameter 'dataName' cannot be used!");
				} else {
					params.put("dataName", dataName);
				}
				response = restTemplate.postForEntity( endpoint, params, String.class);
			}
			
			LOGGER.debug("Called the REST service. The time needed to call it is {} ms. The response is '{}'", Calendar.getInstance().getTimeInMillis() - startTime.getTimeInMillis(), response);
		} catch(org.springframework.web.client.ResourceAccessException e) {
			LOGGER.warn("The endpoint is not available! Calling the next one. {}", e);
			response = callWithLoadBalancing(endpointsQueue.next(), dataName, id, params);
		}

		return response;
	}


//	private Map<String, Object> createLetter(Map<String, Object> bodyMap) {
//		Map<String, Object> letterMap = new HashMap<String, Object>();
//		
//		letterMap.put("body", bodyMap);
//		
//		return letterMap;
//	}

	
	public RestTemplate getRestTemplate() {
		return restTemplate;
	}

	public void setRestTemplate(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	public EndpointsQueue getEndpointsQueue() {
		return endpointsQueue;
	}

	public void setEndpointsQueue(EndpointsQueue endpointsQueue) {
		this.endpointsQueue = endpointsQueue;
	}


}