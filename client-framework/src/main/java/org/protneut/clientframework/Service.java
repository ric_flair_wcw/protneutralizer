package org.protneut.clientframework;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.map.ObjectMapper;
import org.protneut.clientframework.call.RestClient;
import org.protneut.clientframework.exception.ClientFrameworkException;
import org.protneut.clientframework.exception.ServerException;
import org.protneut.common.utils.MapUtils;
import org.protneut.common.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;


public class Service {

	
	private static final ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("clientFramework-applicationContext.xml");
	private static final Logger LOGGER = LoggerFactory.getLogger(Service.class);
	
	private RestClient restClient;
	
	private Service() { }

	
	public static Service createInstance() {
		return (Service) applicationContext.getBean("service");
	}
	
	
	public static void closeApplicationContext() {
		applicationContext.close();
	}
	
	
	public String get(String dataName, String id) throws ClientFrameworkException, RestClientException, ServerException {
		Utils.checkEmpty(dataName, "dataName");
		Utils.checkEmpty(id, "id");
		
		ResponseEntity<String> responseEntity = restClient.forward(dataName, id, null);
		
		checkResponse(responseEntity);
		
		return responseEntity.getBody();
	}
	public String get(String dataName, Map<String, Object> params) {
		return null;
	}

	public <T> T get(String dataName, String id, Class<T> clazz) throws ClientFrameworkException, RestClientException, ServerException {
		return clazz.cast(get(dataName, id));
	}
	public <T> T get(String dataName, Map<String, Object> params, Class<T> clazz) {
		return clazz.cast(get(dataName, params));
	}
	
	
	public Map<String,?> getStruct(String dataName, String id) {
		return null;
	}
	public Map<String,?> getStruct(String dataName, Map<String, Object> params) throws RestClientException, ClientFrameworkException, ServerException {
		Utils.checkEmpty(dataName, "dataName");
		Utils.checkEmpty(params, "params");
		
		ResponseEntity<String> responseEntity = restClient.forward(dataName, null, params);
		
		// getting the content of the response (the element under the 'body')
		Object bodyNode = checkResponse(responseEntity).get("body");
		if (bodyNode == null) {
			LOGGER.info("The arrived resposne is {}", responseEntity.getBody());
			throw new ClientFrameworkException("There isn't 'body' root element in the JSON response!");
		}
		
		if (bodyNode instanceof Map) {
			return (Map<String, ?>) bodyNode;
		} else {
			LOGGER.info("The arrived resposne is {}", responseEntity.getBody());
			throw new ClientFrameworkException("Expected a map as a top element but the top element is not a map!");
		}
	}

	
//	public List<String> getList(String dataName, String id) {
//		return null;
//	}
//	public List<String> getList(String dataName, Map<String, Object> params) {
//		return null;
//	}
//	
//	public <T> List<T> getList(String dataName, String id, Class<T> clazz) {
//		return null;
//	}
//	public <T> List<T> getList(String dataName, Map<String, Object> params, Class<T> clazz) {
//		return null;
//	}
//
//	public List<Map<String,?>> getStructList(String dataName, String id) {
//		return null;
//	}
//	public List<Map<String,?>> getStructList(String dataName, Map<String, Object> params) throws ClientFrameworkException, ServerException {
//		Utils.checkEmpty(dataName, "dataName");
//		Utils.checkEmpty(params, "params");
//		
//		ResponseEntity<String> responseEntity = restClient.forward(dataName, null, params);
//		
//		// getting the content of the response (the element under the 'body')
//		Object bodyNode = checkResponse(responseEntity).get("body");
//		if (bodyNode == null) {
//			LOGGER.info("The arrived resposne is {}", responseEntity.getBody());
//			throw new ClientFrameworkException("There isn't 'body' root element in the JSON response!");
//		}
//		
//		if (bodyNode instanceof List) {
//			return (List<Map<String, ?>>) bodyNode;
//		} else {
//			LOGGER.info("The arrived resposne is {}", responseEntity.getBody());
//			throw new ClientFrameworkException("Expected a list as a top element but the top element is not a list!");
//		}
//	}


	public RestClient getRestClient() {
		return restClient;
	}

	public void setRestClient(RestClient restClient) {
		this.restClient = restClient;
	}

	
	// ************************************************************************************************************
	private Map<String, Object> checkResponse(ResponseEntity<String> responseEntity) throws ClientFrameworkException, ServerException {
		Map<String, Object> rootNode = null;
		if (responseEntity.getHeaders().getContentType().getSubtype().equalsIgnoreCase("json")) {
			ObjectMapper mapper = new ObjectMapper();
			try {
				rootNode = mapper.readValue(responseEntity.getBody(), Map.class);
			} catch (IOException e) {
				throw new ClientFrameworkException("The response is JSON but the root node cannot be read!", e);
			}
			String statusResponse = MapUtils.goToWithoutNamespace(rootNode, "/header/status", String.class);
			
			if (statusResponse != null && statusResponse.equals("error")) {
				String errorClass = MapUtils.goToWithoutNamespace(rootNode, "/body/class", String.class);
				String errorMessage = MapUtils.goToWithoutNamespace(rootNode, "/body/message", String.class);
				throw new ServerException(errorMessage, errorClass);
			}
		}
		return rootNode;
	}


}
