package org.protneut.clientframework;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.protneut.clientframework.config.ConfigurationReaderUtil;
import org.protneut.clientframework.exception.ClientFrameworkException;

public class EndpointsQueue {

	
	private ConfigurationReaderUtil configurationReaderUtil;
	private Queue<String> endpointsQueue = new ConcurrentLinkedQueue<String>();
	private boolean initialized = false;
	
	
	public String next() throws ClientFrameworkException {
		if (!initialized) {
			initQueue();
			initialized = true;
		}
		String endpoint = endpointsQueue.poll();
		return endpoint;
	}


	private void initQueue() throws ClientFrameworkException {
		endpointsQueue.addAll(configurationReaderUtil.getEndpoints());		
	}
	
	
	public ConfigurationReaderUtil getConfigurationReaderUtil() {
		return configurationReaderUtil;
	}

	public void setConfigurationReaderUtil(ConfigurationReaderUtil configurationReaderUtil) {
		this.configurationReaderUtil = configurationReaderUtil;
	}

}
