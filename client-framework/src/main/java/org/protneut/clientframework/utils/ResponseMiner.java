package org.protneut.clientframework.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.protneut.clientframework.exception.ClientFrameworkException;
import org.protneut.common.utils.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ResponseMiner {

	
	private static final Logger LOGGER = LoggerFactory.getLogger(ResponseMiner.class);
			
	
	public static <T> T getSimple(String path, Map<String, ?> mapAll, Class<T> type) throws ClientFrameworkException {
		return getElement(path, mapAll, type);
	}


	public static Map<String, Object> getStruct(String path, Map<String, ?> mapAll) throws ClientFrameworkException {
		return getElement(path, mapAll, Map.class);
	}

	
	public static <T> List<T> getSimpleList(String path, Map<String, ?> mapAll, Class<T> type) throws ClientFrameworkException {
		// getting the list from the map
		List<?> list = getElement(path, mapAll, List.class);
		
		List<T> resultList = new ArrayList<T>();
		// casting the elements of the list to T
		for(Object o : list) {
			try {
				T t = type.cast(o);
				resultList.add(t);
			} catch(ClassCastException cce) {
				throw new ClientFrameworkException("The element '" + o + "' cannot be cast to '" + type + "'!");
			}
		}
		return resultList;
	}


	public static List<Map<String, Object>> getStructList(String path, Map<String, ?> mapAll) throws ClientFrameworkException {
		// getting the list from the map
		List<?> list = getElement(path, mapAll, List.class);
		
		List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
		// casting the elements of the list to T
		for(Object o : list) {
			try {
				Map m = Map.class.cast(o);
				resultList.add(m);
			} catch(ClassCastException cce) {
				throw new ClientFrameworkException("The element '" + o + "' cannot be cast to 'Map'!");
			}
		}
		return resultList;
	}


	private static <T> T getElement(String path, Map<String, ?> mapAll, Class<T> type) throws ClientFrameworkException {
		Map<String, ?> map = mapAll;
		String[] nodes = MapUtils.trimAndSplit(path);
		StringBuffer currentPath = new StringBuffer();
		if (nodes.length > 1) {
			for(int i = 0; i < nodes.length-1; i++) {
				currentPath.append("/").append(nodes[i]);
				if (map.get(nodes[i]) instanceof Map) {
					map = (Map<String, ?>) map.get(nodes[i]);
				} else {
					LOGGER.info("map: {}", mapAll);
					throw new ClientFrameworkException("I expected Map and not " + map.get(nodes[i]).getClass() + " at "+ currentPath.toString() + "!");
				}
			}
		} else if (nodes.length == 0) {
			throw new ClientFrameworkException("The map seems to be empty!");
		}
		String key = nodes[nodes.length-1];
		
		try {
			return type.cast(map.get(key));
		} catch(ClassCastException cce) {
			throw new ClientFrameworkException("The type of the element '" + path + "' in the map is not '" + type + "' but '" + map.get(key).getClass() + "'!");
		}
	}

	
}
