package org.protneut.clientframework.utils;

import java.util.HashMap;
import java.util.Map;

import org.protneut.common.utils.Utils;

public class ParameterBuilder {

	private Map<String, Object> params = null;
	
	
	public ParameterBuilder() {
		params = new HashMap<String, Object>();
	}
	
	
	public void add(String path, Object value) {
		Utils.checkEmpty(path, "path");
		Utils.checkNull(value, "value");
		
		String[] pathNodeNames = path.split("/");
		Map<String, Object> root = params;
		for(int i=0 ; i < pathNodeNames.length-1; i++) {
			if (Utils.isNull(root.get(pathNodeNames[i]))) {
				root.put(pathNodeNames[i], new HashMap<String, Object>());
			}
			root = (Map<String, Object>)root.get(pathNodeNames[i]);
		}
		
		root.put(pathNodeNames[pathNodeNames.length-1], value);
	}

	
	public Map<String, Object> getParams() {
		return params;
	}

}
