package _org.protneut.testclient._xmltomap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.converters.collections.MapConverter;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import com.thoughtworks.xstream.mapper.Mapper;

public class Xstream2 {

	
	public static void main(String[] args) {
		StaxDriver staxDriver = new StaxDriver(/*qmap*/);
        XStream magicApi = new XStream(staxDriver);

        magicApi.registerConverter(new MyMapConverter(magicApi.getMapper()));
        magicApi.alias("response", Map.class);
        magicApi.alias("preferences", Map.class);
        magicApi.alias("preference", String.class);
        System.out.println(  magicApi.fromXML("<response xmlns:ns=\"http://local\"><preferences><ns:preference key=\"miao\">bau</ns:preference><ns:preference key=\"geova\">allah</ns:preference></preferences></response>"));
	}
	
}

class MyMapConverter<T> extends MapConverter {
 
	public MyMapConverter(Mapper mapper) {
		super(mapper);
	}
 
	public boolean canConvert(Class type) {
		return type == HashMap.class;
	}
 
	public void marshal(Object source, HierarchicalStreamWriter writer, MarshallingContext context) {
		
	}
 
	public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
		Map<String, T> map = new HashMap<String, T>();
		populateStringMap(reader, context, map);
 
		return map;
	}
 
	protected void populateStringMap(HierarchicalStreamReader reader, UnmarshallingContext context, Map<String, T> map) {
		while (reader.hasMoreChildren()) {
			reader.moveDown();
			
			String nodeName = reader.getNodeName();
			T value = (T) readItem(reader, context, map);
System.out.println("populateStringMap:"+nodeName+"  "+value);
System.out.println(value.getClass());
			reader.moveUp();
			map.put(nodeName, value);
		}
	}
}
