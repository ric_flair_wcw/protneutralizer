package _org.protneut.testclient._xmltomap;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

import com.ximpleware.AutoPilot;
import com.ximpleware.NavException;
import com.ximpleware.VTDGen;
import com.ximpleware.VTDNav;

public class XmlToMapConverter {

	
	private HierarchialHashMap<String, Object> mapAll = new HierarchialHashMap<String, Object>(null, "");
	private HierarchialHashMap<String, Object> pointer = null;
	private Map<String, String> namespaces = new HashMap<String, String>();
	private Map<String, String> namespacesXPath = null;
	private VTDGen vtdGen;
	
	
	public void start(final String xml, final Map<String, String> namespacesXPath) throws Exception {
		this.namespacesXPath = namespacesXPath;
		vtdGen = new VTDGen();
		vtdGen.setDoc(xml.getBytes());
		vtdGen.parse(true);
		
		VTDNav vtdNav = vtdGen.getNav();
		namespaces.putAll(getNamespaces(vtdNav));
		
		AutoPilot autoPilot = new AutoPilot(vtdNav);
		autoPilot.selectElement("*");
		
		if (autoPilot.iterate()) {
			 String rootNodeName = getNodeName(vtdNav);
			 pointer = mapAll;
			 createMap2("/"+rootNodeName, /*"",*/ autoPilot, vtdNav, true);
		}
	}
	
	
	private String getNodeName(VTDNav vtdNav) throws NavException {
		String nodeName = vtdNav.toString(vtdNav.getCurrentIndex());
		return refreshNamespacePrefix(nodeName);
	}


	private void createMap2(String xpath/*, String parentXpath*/, AutoPilot autoPilot, VTDNav vtdNav, boolean needIterate) throws Exception {
		if (needIterate) {
			HierarchialHashMap<String, Object> newMap = new HierarchialHashMap<String, Object>(pointer, xpath);
			putTheValueToMap(xpath, newMap);
			pointer = newMap;
		}
		
		Integer currentDepth = null;
		while (!needIterate || autoPilot.iterate()) {
			needIterate = true;
			String nodeName = getNodeName(vtdNav);
System.out.println("nodeName="+nodeName);

			if (currentDepth == null) {
				currentDepth = vtdNav.getCurrentDepth();
			}
			// if the iteration goes upward (e.g. from /a/b/c to /a/b)
			if (currentDepth > vtdNav.getCurrentDepth()) {
				for(int i=0; i < currentDepth-vtdNav.getCurrentDepth(); i++) {
					pointer = pointer.getParent();
				}
				createMap2(pointer.getCurrentXpath()/*+"/"+nodeName, pointer.getCurrentXpath()*/, autoPilot, vtdNav, false);
			} else {
				int t = vtdNav.getText();
				if (t != -1) {
					String value = vtdNav.toNormalizedString(t);
System.out.println("    value="+value);
					putTheValueToMap(xpath+"/"+nodeName, value);
				} else {
					if (vtdNav.getContentFragment() != -1) {
						createMap2(xpath+"/"+nodeName/*, xpath*/, autoPilot, vtdNav, true);
					} else {
						putTheValueToMap(xpath+"/"+nodeName, new HashMap<String, String>());
					}
				}
			}
		}
	}
	
	private void putTheValueToMap(String key, Object value) {
		if (pointer.containsKey(key)) {
			List list = null;
			if (pointer.get(key) instanceof List<?> ) {
				list = (List<?>)pointer.get(key);
			} else {
				list = new CopyOnWriteArrayList<String>();
				list.add(pointer.get(key));
				pointer.remove(key);
			}
			list.add(value);
			pointer.put(key, list);
		} else {
			pointer.put(key, value);
		}
	}
	

	private String refreshNamespacePrefix(String nodeName) {
		if (nodeName.indexOf(":") != -1) {
			String namespacePrefix = nodeName.substring(0, nodeName.indexOf(":"));
			String theRestOfTheNodeName = nodeName.substring(nodeName.indexOf(":") + 1);
			String namespaceXml = namespaces.get(namespacePrefix);
			String namespaceXPathPrefix = namespaceXPathPrefix(namespaceXml);
			return namespaceXPathPrefix + ":" + theRestOfTheNodeName;
		} else {
			return nodeName;
		}
		
	}


	private String namespaceXPathPrefix(String namespaceXml) {
		for(String namespaceXPathPrefix : namespacesXPath.keySet()) {
			if (namespacesXPath.get(namespaceXPathPrefix).equals(namespaceXml)) {
				return namespaceXPathPrefix;
			}
		}
		throw new RuntimeException("It is pretty unexpected... The '"+namespaceXml+"' namespace is not in the list of namespaces coming from extraction.");
	}


	private Map<String, String> getNamespaces(final VTDNav vtdNav) throws Exception {
		Map<String, String> ns = new HashMap<String, String>();
		AutoPilot ap = new AutoPilot(vtdNav);
		ap.selectXPath("/*/namespace::*");
		int i=-1;
		while((i=ap.evalXPath())!=-1){
			// i will be attr name, i+1 will be attribute value
			// to get rid if the 'xmlns:' text in the namespace prefix
			String key = vtdNav.toNormalizedString(i);
			if (key.indexOf(":") != -1) {
				key = key.substring(key.indexOf(":")+1);
			}
			
			ns.put(key, vtdNav.toNormalizedString(i+1));
		}
		return ns;
	}


	public HierarchialHashMap<String, Object> getMap() {
		return mapAll;
	}


}
