package _org.protneut.testclient._xmltomap;

import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Map;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.converters.collections.MapConverter;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.thoughtworks.xstream.io.xml.QNameMap;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import com.thoughtworks.xstream.mapper.Mapper;

public class XstreamTest {

	
	public static void main(String[] args) {
//		String xml="<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ns=\"http://virginmedia/schema/GetAddressForPostCode/4/0\"  xmlns:ns1=\"http://virginmedia/schema/RequestHeader/1/4\" xmlns:ns2=\"http://virginmedia/schema/BaseType/1/3\"><soapenv:Header/><soapenv:Body><ns:GetAddressForPostCodeRequest><ns:requestHeader><ns1:id>?</ns1:id><ns1:source>?</ns1:source><ns1:user><ns2:userId>?</ns2:userId><ns2:credentials>?</ns2:credentials></ns1:user></ns:requestHeader><ns:postalAddress><ns:buildingName>${protneutParam.buildingName}</ns:buildingName><ns:country>${protneutParam.country}</ns:country><ns:postcode>${protneutParam.postcode}</ns:postcode><ns:addressIdentification><ns:identifierType>${protneutParam.addressIdentification.identifierType}</ns:identifierType><ns:addressIdentifier>${protneutParam.addressIdentification.addressIdentifier}</ns:addressIdentifier></ns:addressIdentification></ns:postalAddress></ns:GetAddressForPostCodeRequest></soapenv:Body></soapenv:Envelope>";
		String xml="<Envelope><Body><GetAddressForPostCodeRequest>a</GetAddressForPostCodeRequest></Body></Envelope>";
		
//		Map<String,String> map = new HashMap<String,String>();
//        map.put("name","chris");
//        map.put("island","faranga");

//		QNameMap qmap = new QNameMap();
//		qmap.setDefaultNamespace("http://schemas.xmlsoap.org/soap/envelope/");
//		qmap.setDefaultPrefix("soapenv");
		StaxDriver staxDriver = new StaxDriver(/*qmap*/);
        XStream magicApi = new XStream(staxDriver);
        magicApi.registerConverter(new MapEntryConverter(magicApi.getMapper()));
//        magicApi.registerConverter(new com.thoughtworks.xstream.converters.collections.TreeMapConverter(magicApi.getMapper()));
//        magicApi.registerConverter(new MapConverter(magicApi.getMapper()) {
//            public boolean canConvert(Class type) {
//                return type == Map.class;
//            }
//        });
//        magicApi.addDefaultImplementation(HashMap.class, Map.class);
        magicApi.alias("root", Map.class);
        magicApi.alias("a", Map.class);
        magicApi.alias("b", String.class);
        

//        String xml = magicApi.toXML(map);
//        System.out.println("Result of tweaked XStream toXml()");
//        System.out.println(xml);

        //Map<String, Object> extractedMap = (Map<String, Object>) magicApi.fromXML("<root>"+xml+"</root>");
        System.out.println(magicApi.fromXML("<root><a><b>b</b></a></root>").toString());
        
//		System.out.println(extractedMap);
	}
	
	
	public static class MapEntryConverter extends MapConverter {

        public MapEntryConverter(Mapper mapper) {
			super(mapper);
		}

		public boolean canConvert(Class clazz) {
            return AbstractMap.class.isAssignableFrom(clazz);
        }

        public void marshal(Object value, HierarchicalStreamWriter writer, MarshallingContext context) {
System.out.println("marshal:"+value);
            AbstractMap map = (AbstractMap) value;
            for (Object obj : map.entrySet()) {
                Map.Entry entry = (Map.Entry) obj;
                writer.startNode(entry.getKey().toString());
                writer.setValue(entry.getValue().toString());
                writer.endNode();
            }

        }

        public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {

            Map<String, Object> map = new HashMap<String, Object>();

            while(reader.hasMoreChildren()) {
System.out.println("unmarshal:"+reader.getNodeName());
                reader.moveDown();

                String key = reader.getNodeName(); // nodeName aka element's name
                String value = reader.getValue();
                map.put(key, value);

                reader.moveUp();
            }

            return map;
        }

    }	
}
