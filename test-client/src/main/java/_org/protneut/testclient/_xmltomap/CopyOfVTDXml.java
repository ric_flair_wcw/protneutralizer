package _org.protneut.testclient._xmltomap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ximpleware.AutoPilot;
import com.ximpleware.EOFException;
import com.ximpleware.EncodingException;
import com.ximpleware.EntityException;
import com.ximpleware.NavException;
import com.ximpleware.ParseException;
import com.ximpleware.PilotException;
import com.ximpleware.VTDGen;
import com.ximpleware.VTDNav;
import com.ximpleware.XPathParseException;

public class CopyOfVTDXml {

	private HierarchialHashMap<String, Object> mapAll = new HierarchialHashMap<String, Object>(null, "");
	private HierarchialHashMap<String, Object> pointer = null;
	private Map<String, String> namespaces = new HashMap<String, String>();
	private VTDGen vtdGen;
	
	public static void main(String[] args) throws Exception {
		//String xml = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ns=\"http://virginmedia/schema/GetAddressForPostCode/4/0\"  xmlns:ns1=\"http://virginmedia/schema/RequestHeader/1/4\" xmlns:ns2=\"http://virginmedia/schema/BaseType/1/3\"><soapenv:Header/><soapenv:Body><ns:GetAddressForPostCodeRequest><ns:requestHeader><ns1:id>?</ns1:id><!--Optional:--><ns1:source>?</ns1:source><ns1:user><ns2:userId>1</ns2:userId><ns2:credentials>1</ns2:credentials></ns1:user><ns1:user><ns2:userId>2</ns2:userId><ns2:credentials>2</ns2:credentials></ns1:user></ns:requestHeader><!--Optional:--><ns:postalAddress><!--Optional:--><ns:buildingName>A</ns:buildingName><!--Optional:--><ns:country>B</ns:country><!--!!!!!!!!MANDATORY!!!!!!!!!!--><ns:postcode>C</ns:postcode><!--Optional:--><ns:addressIdentification><ns:identifierType>D</ns:identifierType><ns:addressIdentifier>E</ns:addressIdentifier><!--Optional:--><!--ns:instanceIdentifier>?</ns:instanceIdentifier--></ns:addressIdentification></ns:postalAddress><ns:postalAddress><!--Optional:--><ns:buildingName>A2</ns:buildingName><!--Optional:--><ns:country>B2</ns:country><!--!!!!!!!!MANDATORY!!!!!!!!!!--><ns:postcode>C2</ns:postcode><!--Optional:--><ns:addressIdentification><ns:identifierType>D2 1</ns:identifierType><ns:addressIdentifier>E2 1</ns:addressIdentifier></ns:addressIdentification><ns:addressIdentification><ns:identifierType>D2 2</ns:identifierType><ns:addressIdentifier>E2 2</ns:addressIdentifier></ns:addressIdentification><ns:addressIdentification><ns:identifierType>D2 3</ns:identifierType><ns:addressIdentifier>E2 3</ns:addressIdentifier></ns:addressIdentification></ns:postalAddress></ns:GetAddressForPostCodeRequest></soapenv:Body></soapenv:Envelope>";
//		String xml = "<a><b><c>1</c><c>2</c></b><b><c>3</c><c>4</c><c>5</c></b></a>";
		String xml = "<a xmlns:egy=\"http://egy\" xmlns:ketto=\"http://ketto\"><ketto:b><c>1</c><c>2</c><d><egy:e/></d><d><egy:e>6</egy:e></d><d><egy:e>7</egy:e><egy:e>8</egy:e><egy:e>9</egy:e><egy:e>10</egy:e></d></ketto:b><ketto:b><c>3</c><c>4</c><c>5</c></ketto:b><ketto:b><c><d><egy:e/><egy:e>11</egy:e></d></c></ketto:b></a>";
		CopyOfVTDXml vtd = new CopyOfVTDXml();
		
		vtd.start(xml);
		
		// refreshNamespaces()
		
		// removeUnnecessaryNodes()
		
		System.out.println(vtd.mapAll);
	}
	
	public void start(final String xml) throws Exception {
		
		vtdGen = new VTDGen();
		vtdGen.setDoc(xml.getBytes());
		vtdGen.parse(true);
		
		VTDNav vtdNav = vtdGen.getNav();
		namespaces.putAll(getNamespaces(vtdNav));
		
		AutoPilot autoPilot = new AutoPilot(vtdNav);
		autoPilot.selectElement("*");
		
		if (autoPilot.iterate()) {
			 String rootNodeName = vtdNav.toString(vtdNav.getCurrentIndex());
			 pointer = mapAll;
			 createMap2("/"+rootNodeName, "", autoPilot, vtdNav);
		}
		
	}
	
	
	private void createMap2(String xpath, String parentXpath, AutoPilot autoPilot, VTDNav vtdNav) throws Exception {
System.out.println("createMap2  " + xpath);
		HierarchialHashMap<String, Object> newMap = new HierarchialHashMap<String, Object>(pointer, xpath);
		putTheValueToMap(xpath, newMap);
		pointer = newMap;
		
		Integer currentDepth = null;
		while (autoPilot.iterate()) {
			String nodeName = vtdNav.toString(vtdNav.getCurrentIndex());

			if (currentDepth == null) {
				currentDepth = vtdNav.getCurrentDepth();
			}
System.out.println(nodeName + "  " + currentDepth + " ?=" + vtdNav.getCurrentDepth());
			// if the iteration goes upward (e.g. from /a/b/c to /a/b)
			if (currentDepth > vtdNav.getCurrentDepth()) {
				for(int i=0; i < currentDepth-vtdNav.getCurrentDepth(); i++) {
					pointer = pointer.getParent();
				}
				createMap2(pointer.getCurrentXpath()+"/"+nodeName, xpath, autoPilot, vtdNav);
			} else {
				int t = vtdNav.getText();
System.out.println("t="+t);
				if (t != -1) {
					String value = vtdNav.toNormalizedString(t);
System.out.println("value="+value);
					putTheValueToMap(xpath+"/"+nodeName, vtdNav.toNormalizedString(t));
				} else {
System.out.println(vtdNav.getContentFragment());
					if (vtdNav.getContentFragment() != -1) {
//System.out.println("vtdNav.getContentFragment() != -1  "+xpath);
						createMap2(xpath+"/"+nodeName, xpath, autoPilot, vtdNav);
					} else {
						putTheValueToMap(xpath+"/"+nodeName, new HashMap<String, String>());
					}
				}
			}
		}
	}
	
	private void putTheValueToMap(/*Map<String, Object> map, */String key, Object value) {
		if (pointer.containsKey(key)) {
			List list = null;
			if (pointer.get(key) instanceof List<?> ) {
				list = (List<?>)pointer.get(key);
			} else {
				list = new ArrayList<String>();
				list.add(pointer.get(key));
				pointer.remove(key);
			}
			list.add(value);
			pointer.put(key, list);
		} else {
			pointer.put(key, value);
		}
	}

	private Map<String, String> getNamespaces(final VTDNav vtdNav) throws Exception {
		Map<String, String> ns = new HashMap<String, String>();
		AutoPilot ap = new AutoPilot(vtdNav);
		ap.selectXPath("/*/namespace::*");
		  int i=-1;
		  while((i=ap.evalXPath())!=-1){
			// i will be attr name, i+1 will be attribute value
			ns.put(vtdNav.toNormalizedString(i), vtdNav.toNormalizedString(i+1));
		}
		return ns;
	}
	
class HierarchialHashMap<K,V> extends HashMap<K, V> {
	
	private HierarchialHashMap<K, V> parent = null;
	private String currentXpath;


	public HierarchialHashMap(HierarchialHashMap<K,V> parent, String currentXpath) {
//System.out.println("HierarchialHashMap - currentXpath:"+currentXpath+"  parent:"+parent);
		this.parent = parent;
		this.currentXpath = currentXpath;
	}

	
	public HierarchialHashMap<K, V> getParent() {
		return parent;
	}

	public String getCurrentXpath() {
		return currentXpath;
	}

	@Override
	public String toString() {
		return super.toString();
	}
}

}
