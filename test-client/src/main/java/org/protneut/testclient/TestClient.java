package org.protneut.testclient;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.protneut.clientframework.Service;
import org.protneut.clientframework.exception.ClientFrameworkException;
import org.protneut.clientframework.exception.ServerException;
import org.protneut.clientframework.utils.ParameterBuilder;
import org.protneut.clientframework.utils.ResponseMiner;
import org.springframework.web.client.RestClientException;

public class TestClient {

	
	public void get_return_String__String() throws  RestClientException, ClientFrameworkException, ServerException{
		String customerId = "ksdf87432hjdsf879";
		
		String customerName = Service.createInstance().get("customer->name", customerId);
		
		System.out.println("customerName:"+customerName);
	}
	
	
	public void get_return_String__Long() throws ClientFrameworkException, RestClientException, ServerException {
		Long customerId = 9786239873259807l;
		
		String customerName = Service.createInstance().get("customer->name", customerId.toString());
	}
	
	
	public void get_return_Integer__String() throws ClientFrameworkException, RestClientException, ServerException {
		String customerId = "ksdf87432hjdsf879";
		
		//Integer periodOfmembership = service.get("periodOfmembership", customerId);
		Integer periodOfmembership = Service.createInstance().get("customer->membership->period", customerId, Integer.class);
	}
	
	
//	public void get_return_ListOfString__Map() {
//		ParameterBuilder builder = new ParameterBuilder();
//		builder.add("/customer/location", "Bournemouth");
//		builder.add("/customer/startDate", Calendar.getInstance());
//		builder.add("/customer/endDate", Calendar.getInstance());
//		
//		// the customers who became clients between two dates and they are from Bournemouth
//		List<String> customerNames = Service.createInstance().getList("customer->names@membershipBetween", builder.getParams());
//	}
	
	
	public void get_return_Map__String() throws ClientFrameworkException {
		String customerId = "ksdf87432hjdsf879";
		
		Map<String, ?> customerAddress = Service.createInstance().getStruct("customer->addressFull", customerId);
		
		String contact_postcode = (String) ((Map<String, ?>)customerAddress.get("contactAddress")).get("postcode");
		contact_postcode = ResponseMiner.getSimple("/contactAddress/postcode", customerAddress, String.class);
	}

	
	public void get_return_ListOfMap__Map() throws ClientFrameworkException, ServerException {
		ParameterBuilder builder = new ParameterBuilder();
		builder.add("postcode", "BH1 3EH");	// MANDATORY
		builder.add("country", "HU");  // optional
		builder.add("addressIdentification/identifierType", "nemtom");  // optional
		builder.add("addressIdentification/addressIdentifier", "eztse");  // optional
		
		// the customers who became clients between two dates and they are from Bournemouth
		//		address : the theoretical data name
		//		getAddressForPostCode : the service name
		//		postalAddress_short : the name of the datagroup in address->getAddressForPostCode
		Map<String, ?> adresses = Service.createInstance().getStruct("address->getAddressForPostCode->postalAddress_short", builder.getParams());
		List<Map<String, Object>> postalAddresses = ResponseMiner.getStructList("/soapenv:Envelope/soapenv:Body/ns:GetAddressForPostCodeResponse/ns:postalAddress", adresses);
		System.out.println(postalAddresses);
	}

	
	public void get_return_Map__Map() throws RestClientException, ClientFrameworkException, ServerException {
		ParameterBuilder builder = new ParameterBuilder();
		builder.add("postcode", "BH1 3EH");	// MANDATORY
		builder.add("country", "HU");  // optional
		builder.add("addressIdentification/identifierType", "nemtom");  // optional
		builder.add("addressIdentification/addressIdentifier", "eztse");  // optional
		
		Map<String, ?> customerAddress = Service.createInstance().getStruct("address->getAddressForPostCode->postalAddress_short", builder.getParams());
		
		List<String> identifierTypes = ResponseMiner.getSimpleList("/soapenv:Envelope/soapenv:Body/ns:GetAddressForPostCodeResponse/ns:postalAddress/ns:addressIdentification/ns:identifierType", customerAddress, String.class);
		System.out.println(identifierTypes);
	}

	
	public static void main(String[] args) throws ClientFrameworkException, RestClientException, ServerException {
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS");
		System.out.println("Start - " + dateFormat.format(new Date()));
		
//		new TestClient().get_return_String__String();
//		new TestClient().get_return_ListOfMap__Map();
//		new TestClient().get_return_Map__Map();
		new TestClient().get_return_ListOfMap__Map();
		
		System.out.println("End - " + dateFormat.format(new Date()));
	}
}
