package org.protneut.testclient.extract;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.protneut.server.beans.workflow.WorkflowBean;
import org.protneut.server.common.exceptions.ExternalServiceException;
import org.protneut.server.extract.ResponseExtractor;
import org.protneut.server.extract.XmlToMapConverter;

import com.ximpleware.EOFException;
import com.ximpleware.EncodingException;
import com.ximpleware.EntityException;
import com.ximpleware.NavException;
import com.ximpleware.ParseException;
import com.ximpleware.XPathEvalException;
import com.ximpleware.XPathParseException;

public class TestResponseExtractor {

	public static void main(String[] args) throws EncodingException, EOFException, EntityException, NavException, ParseException, XPathParseException, XPathEvalException, ExternalServiceException {
		String responseString = "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/1999/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/1999/XMLSchema\"><SOAP-ENV:Body><SOAP-ENV:Fault><faultcode xsi:type=\"xsd:string\">SOAP-ENV:Client</faultcode><faultstring xsi:type=\"xsd:string\">It is a very seroius fault! really it is...</faultstring></SOAP-ENV:Fault></SOAP-ENV:Body></SOAP-ENV:Envelope>";
		
		List<Map<String, String>> extractsListMap = new ArrayList<Map<String,String>>();
		Map<String, String> extract = new HashMap<String, String>();
		extract = new HashMap<String, String>();
		extract.put("type", "xpath");
		extract.put("from", "/soapenv:Envelope/soapenv:Body/ns:GetAddressForPostCodeResponse/ns:postalAddress/ns:displayAddress");
		extractsListMap.add(extract);
		extract = new HashMap<String, String>();
		extract.put("type", "xpath");
		extract.put("from", "/soapenv:Envelope/soapenv:Body/ns:GetAddressForPostCodeResponse/ns:postalAddress/ns:buildingName");
		extract = new HashMap<String, String>();
		extract.put("type", "xpath");
		extract.put("from", "/soapenv:Envelope/soapenv:Body/ns:GetAddressForPostCodeResponse/ns:postalAddress/ns:addressIdentification/ns:identifierType");
		extractsListMap.add(extract);
		
		Map<String, String> namespaces = new HashMap<String, String>();
		namespaces.put("SOAP-ENV", "http://schemas.xmlsoap.org/soap/envelope/");
		namespaces.put("xsi", "http://www.w3.org/1999/XMLSchema-instance");
		namespaces.put("xsd", "http://www.w3.org/1999/XMLSchema");
		
		WorkflowBean bean = new WorkflowBean(null, null, null, null, responseString, null, null, extractsListMap, null, null, namespaces);
		
		ResponseExtractor extractor = new ResponseExtractor();
		extractor.setXmlToMapConverter(new XmlToMapConverter());
		extractor.extract(bean);
	}
}
