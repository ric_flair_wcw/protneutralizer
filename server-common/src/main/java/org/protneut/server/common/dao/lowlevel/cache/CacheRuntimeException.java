package org.protneut.server.common.dao.lowlevel.cache;

public class CacheRuntimeException extends RuntimeException {

	private static final long serialVersionUID = 8985642201800110331L;

	public CacheRuntimeException(String message, Throwable t) {
		super(message, t);
	}

}
