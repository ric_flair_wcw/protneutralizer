package org.protneut.server.common.dao.highlevel.cache;

import org.protneut.server.common.beans.cache.DataCacheBean;
import org.protneut.server.common.beans.cache.ServiceCacheBean;
import org.protneut.server.common.dao.lowlevel.cache.ICache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CacheBeanDAOImpl implements ICacheBeanDAO {

	
	private static final Logger LOGGER = LoggerFactory.getLogger(CacheBeanDAOImpl.class);
	
	private ICache cache = null;
	
	
	public DataCacheBean getDataCacheBean(String dataName) {
//		LOGGER.debug("getDataCacheBean(dataName->'{}')", dataName);
		return cache.get(dataName, DataCacheBean.class);
	}

	public void put(String dataName, DataCacheBean cacheBean) {
//		LOGGER.debug("put(dataName->'{}', dataCacheBean->'{}')", dataName, cacheBean);
		cache.put(dataName, cacheBean);
	}

	
	public ServiceCacheBean getServiceDataCacheBean(String serviceName) {
//		LOGGER.debug("getServiceDataCacheBean(serviceName->'{}')", serviceName);
		return cache.get(serviceName, ServiceCacheBean.class);
	}

	public void put(String serviceName, ServiceCacheBean cacheBean) {
//		LOGGER.debug("put(serviceName->'{}', serviceCacheBean->'{}')", serviceName, cacheBean);
		cache.put(serviceName, cacheBean);
	}

	
	public ICache getCache() {
		return cache;
	}
	public void setCache(ICache cache) {
		this.cache = cache;
	}


}
