package org.protneut.server.common.beans.cache;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class ServiceCacheBean implements Serializable {

	private static final long serialVersionUID = 9032720325288699720L;
	
	private String name;
	private String type;
	private String requestString;
	private List<String> endpoints;
	private String soapAction;
	private Map<String,String> namespaces;
	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getRequestString() {
		return requestString;
	}

	public void setRequestString(String requestString) {
		this.requestString = requestString;
	}

	public List<String> getEndpoints() {
		return endpoints;
	}

	public void setEndpoints(List<String> endpoints) {
		this.endpoints = endpoints;
	}

	public String getSoapAction() {
		return soapAction;
	}

	public void setSoapAction(String soapAction) {
		this.soapAction = soapAction;
	}

	public Map<String, String> getNamespaces() {
		return namespaces;
	}

	public void setNamespaces(Map<String, String> namespaces) {
		this.namespaces = namespaces;
	}


	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(this.getClass().getName()).append("[");
		sb.append("name=").append(name);
		sb.append(", type=").append(type);
		sb.append(", requestString=").append(requestString);
		sb.append(", endpoints=").append(endpoints);
		sb.append(", soapAction=").append(soapAction);
		sb.append(", namespaces=").append(namespaces);
		sb.append("]");
		return sb.toString();
	}
	
}
