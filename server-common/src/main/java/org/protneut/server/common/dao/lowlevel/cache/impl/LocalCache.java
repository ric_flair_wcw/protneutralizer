package org.protneut.server.common.dao.lowlevel.cache.impl;


import java.net.URL;
import java.util.List;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.protneut.server.common.dao.lowlevel.cache.CacheRuntimeException;
import org.protneut.server.common.dao.lowlevel.cache.ICache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;


/**
 * @author Viktor Horvath
 */
public class LocalCache implements ICache {

	
	private static final Logger LOGGER = LoggerFactory.getLogger(LocalCache.class);
	private static final String LOCAL_CACHE_NAME = "LOCAL_PROTNEUT_STORE";

	
	public void lock(String key) {
		LOGGER.trace(String.format("####### LocalCache.lock(%s)...", key));
		getCache().acquireWriteLockOnKey(key);
		LOGGER.trace(String.format("####### Getting the LocalCache.lock(%s) was successful!", key));
	}

	
	public void unlock(String key) {
		LOGGER.trace(String.format("####### LocalCache.unlock(%s)", key));
		
		try {
			getCache().releaseWriteLockOnKey(key);
		} catch(Exception e) {
			LOGGER.warn(String.format("####### The LocalCache lock cannot be released! key = %s, reason = %s", key, ""+e));
		}
	}

	
	public <T> T get(String key, Class<T> type) {
		T value = null;
		try {
			Element element = getCache().get(key);
			if (element == null) {
				return null;
			} else {
				try {
					// deserialize from JSON
					Gson gson = new Gson();
					value = gson.fromJson((String)element.getObjectValue(), type);
					return value;
				} catch(ClassCastException cce) {
					throw new CacheRuntimeException(String.format("The type of the element '%s' is not %s! it was: %s", 
							key, type, element.getObjectValue().getClass()), cce);
				}
			}
		} finally {
			LOGGER.trace(String.format("####### LocalCache.get(%s, %s) = %s", key, type, value));
		}
	}

	
	public void put(String key, Object value) {
		LOGGER.trace(String.format("####### LocalCache.put(%s, %s)", key, value));
		// serialize to JSON
		Gson gson = new Gson();
		String jsonString = gson.toJson(value);
		// put into the cache		
		getCache().put(new Element(key, jsonString));
	}
	
	
	public List<String> getKeys() {
		LOGGER.trace("####### LocalCache.getKeys()");
		List<String> keys = getCache().getKeys();
		LOGGER.trace("####### keys="+keys);
		return keys;
	}

	
	public void remove(String key) {
		LOGGER.trace(String.format("####### LocalCache.remove(%s)", key));
		getCache().remove(key);
	}

	
	private Cache getCache() {
		// the name of the ehcache should be able to be configured in the general config XML
		URL url = getClass().getResource("/protneut-local-ehcache.xml");
		CacheManager manager = CacheManager.create(url);
		
		Cache cache = manager.getCache(LOCAL_CACHE_NAME);
		return cache;
	}

	
}
