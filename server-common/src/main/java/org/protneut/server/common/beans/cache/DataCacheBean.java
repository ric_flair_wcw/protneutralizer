package org.protneut.server.common.beans.cache;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class DataCacheBean implements Serializable {

	private static final long serialVersionUID = 9032720325288699720L;
	
	private String dataName;
	private String serviceName;
	private List<Map<String,String>> extracts; // 1st - type (e.g. xpath), 2nd - expression (e.g. /customer/nameFull)
	
	
	public String getDataName() {
		return dataName;
	}

	public void setDataName(String dataName) {
		this.dataName = dataName;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public List<Map<String, String>> getExtracts() {
		return extracts;
	}

	public void setExtracts(List<Map<String, String>> extracts) {
		this.extracts = extracts;
	}


	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(this.getClass().getName()).append("[");
		sb.append("dataName=").append(dataName);
		sb.append(", serviceName=").append(serviceName);
		sb.append(", extracts=").append(extracts);
		sb.append("]");
		return sb.toString();
	}
	
}
