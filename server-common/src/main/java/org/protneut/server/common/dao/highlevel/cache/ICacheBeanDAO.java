package org.protneut.server.common.dao.highlevel.cache;

import org.protneut.server.common.beans.cache.DataCacheBean;
import org.protneut.server.common.beans.cache.ServiceCacheBean;

public interface ICacheBeanDAO {
	
	public DataCacheBean getDataCacheBean(String dataName);
	
	public void put(String dataName, DataCacheBean cacheBean);
	
	public ServiceCacheBean getServiceDataCacheBean(String serviceName);

	public void put(String serviceName, ServiceCacheBean cacheBean);
	
}
