package org.protneut.server.common.exceptions;

public class ExternalServiceException extends Exception {

	private static final long serialVersionUID = -2884900967606546978L;

	public ExternalServiceException(String message) {
		super(message);
	}

	public ExternalServiceException(String message, Throwable t) {
		super(message, t);
	}
}
